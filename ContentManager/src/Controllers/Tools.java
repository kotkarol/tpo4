package Controllers;

import ContentManager.Main;
import Models.DataTransferObject;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Tools {

    public enum MessageCodes {
        GetAllTopics,
        DeleteTopic,
        AddNewTopic,
        GetAllFeeds,
        DeleteFeed,
        AddNewFeed
    }

    private static String hostName = "localhost";
    private static int port = 8090;


    public static void SendMessageToServer(MessageCodes messageCode, Object transferObject) throws IOException, InterruptedException {
        InetSocketAddress hostAddress = new InetSocketAddress(hostName, port);
        if (!isHostAvailaible(hostName, port)) return;
        DataTransferObject dto;

        switch (messageCode) {
            case GetAllTopics:
                dto = new DataTransferObject(1, true);
                break;
            case DeleteTopic:
                dto = new DataTransferObject(2, transferObject, true);
                break;
            case AddNewTopic:
                dto = new DataTransferObject(3, transferObject, true);
                break;
            case GetAllFeeds:
                dto = new DataTransferObject(4, true);
                break;
            case DeleteFeed:
                dto = new DataTransferObject(5, transferObject, true);
                break;
            case AddNewFeed:
                dto = new DataTransferObject(6, transferObject, true);
                break;
            default:
                return;
        }
        ByteBuffer buffer = ByteBuffer.wrap(convertToBytes(dto));
        Main.client.write(buffer);
        buffer.clear();
    }

    public static boolean isHostAvailaible(String serverAddress, int serverPort) {
        try (Socket s = new Socket(serverAddress, serverPort)) {
            return true;
        } catch (IOException ex) {
            /* ignore */
        }
        return false;
    }

    public static String getMessageType(String message) {
        StringBuilder allDigits = new StringBuilder();
        for (char c : message.toCharArray()) {
            if (Character.isDigit(c)) allDigits.append(c);
            if (c == ')') break;
        }
        return allDigits.toString();
    }

    public static byte[] convertToBytes(Object object) throws IOException {
        try (
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        }
    }

    public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }
}
