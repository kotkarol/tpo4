package Controllers;

import ContentManager.Main;
import Models.AppFeed;
import Models.DataTransferObject;
import Models.Topic;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import javax.xml.crypto.Data;
import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

public class MainWindowController {
    @FXML
    private javafx.scene.control.Button TopicButton;
    @FXML
    private javafx.scene.control.Button FeedButton;
    @FXML
    private AnchorPane TopicPane;
    @FXML
    private AnchorPane FeedPane;
    @FXML
    private ComboBox<Topic> TopicComboBox;
    @FXML
    private javafx.scene.control.TextField NewTopicNameField;
    @FXML
    private TableView<Topic> TopicTable;
    @FXML
    public TableColumn<Topic, Integer> id;
    @FXML
    public TableColumn<Topic, String> name;
    @FXML
    private TextField FeedTitle;
    @FXML
    private TextArea FeedContent;
    @FXML
    private TableView<AppFeed> FeedTable;
    @FXML
    public TableColumn<Topic, Integer> feedId;
    @FXML
    public TableColumn<Topic, String> feedName;

    private ArrayList<Topic> topics = new ArrayList<>();
    private ArrayList<AppFeed> appFeeds = new ArrayList<>();

    @FXML
    public void initialize() throws IOException, InterruptedException {
        new Thread(getRunnableForServer()).start();
        Thread.sleep(1000);
        id.setCellValueFactory(new PropertyValueFactory<>("Id"));
        name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        feedId.setCellValueFactory(new PropertyValueFactory<>("Id"));
        feedName.setCellValueFactory(new PropertyValueFactory<>("FeedTitle"));
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllTopics, "");
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllFeeds, "");

        //add your data to the table here.
    }

    @FXML
    private void OnTopicButtonClick() {
        TopicPane.setVisible(true);
        FeedPane.setVisible(false);
    }

    @FXML
    private void OnFeedButtonClicked() {
        TopicPane.setVisible(false);
        FeedPane.setVisible(true);
    }

    @FXML
    private void OnNewTopicButtonClicked() throws IOException, InterruptedException {
        String newTopicName = NewTopicNameField.getText();
        if (newTopicName == null || newTopicName.isEmpty()) return;
        else NewTopicNameField.setText("");

        int topicId = getMaxIdFromTopics();
        if (topicId == -1) return;
        Topic topic = new Topic(topicId, newTopicName);

        Tools.SendMessageToServer(Tools.MessageCodes.AddNewTopic, topic);
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllTopics, "");

    }

    public int getMaxIdFromFeeds() {
        for (int i = 0; i <= appFeeds.size(); i++) {
            int finalI = i;
            boolean matched = appFeeds.stream().anyMatch(x -> x.getId() == finalI);
            if (matched) continue;
            return i;
        }
        return -1;
    }

    public int getMaxIdFromTopics() {
        for (int i = 0; i <= topics.size(); i++) {
            int finalI = i;
            boolean matched = topics.stream().anyMatch(x -> x.getId() == finalI);
            if (matched) continue;
            return i;
        }
        return -1;
    }

    @FXML
    private void DeleteTopicButtonClicked() throws IOException, InterruptedException {
        Topic currentServerTopic = TopicTable.getSelectionModel().getSelectedItem();
        if (currentServerTopic == null) return;
        Tools.SendMessageToServer(Tools.MessageCodes.DeleteTopic, currentServerTopic);
        topics.remove(currentServerTopic);
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllTopics, "");
    }

    @FXML
    private void TopicComboBoxSetItems() throws IOException, InterruptedException {
        TopicComboBox.setItems(FXCollections.observableArrayList(topics));
    }

    public void OnFeedDeleteButtonClicked() throws IOException, InterruptedException {
        AppFeed currentServerTopic = FeedTable.getSelectionModel().getSelectedItem();
        if (currentServerTopic == null) return;
        Tools.SendMessageToServer(Tools.MessageCodes.DeleteFeed, currentServerTopic);
        appFeeds.remove(currentServerTopic);
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllFeeds, "");
    }

    public void OnFeedAddButtonClicked() throws IOException, InterruptedException {
        Topic newFeedTopicName = TopicComboBox.getSelectionModel().getSelectedItem();
        String newFeedTitle = FeedTitle.getText();
        String newFeedContent = FeedContent.getText();

        int maxId = getMaxIdFromFeeds();
        if (maxId == -1) return;

        AppFeed appFeed = new AppFeed(maxId, newFeedTopicName, newFeedTitle, newFeedContent);

        Tools.SendMessageToServer(Tools.MessageCodes.AddNewFeed, appFeed);
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllFeeds, "");
        FeedTitle.setText("");
        FeedContent.setText("");
    }

    private Runnable getRunnableForServer() {
        return () -> {
            try {
                InetSocketAddress hostAddress = new InetSocketAddress("localhost", 8090);
                Main.client = SocketChannel.open(hostAddress);
                while (true) {
                    ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
                    if (!Main.client.isConnected() || !Main.client.isOpen()) continue;
                    while (Main.client.read(byteBuffer) > 0) {
                        String message = new String(byteBuffer.array());
                        String messageTypeCode = Tools.getMessageType(message);
                        Object objectFromServer = convertFromBytes(byteBuffer.array());
                        boolean endTransmission = ProcessObjectFromServer(objectFromServer, byteBuffer);
                        if(endTransmission) break;
                    }
                }
            } catch (IOException | InterruptedException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        };
    }

    private boolean ProcessObjectFromServer(Object objectFromServer, ByteBuffer byteBuffer) throws IOException, InterruptedException {

        if(objectFromServer instanceof ArrayList<?>)
        {
            boolean retValue = false;
            ArrayList<DataTransferObject> arrayList = (ArrayList<DataTransferObject>) objectFromServer;
            for (DataTransferObject dto : arrayList) {
                ProcessDto(dto, byteBuffer);
                if(dto.isEndTransmission()) retValue = true;
            }
            return retValue;
        }
        else if(objectFromServer instanceof DataTransferObject)
        {
            DataTransferObject dto = (DataTransferObject)objectFromServer;
            ProcessDto(dto, byteBuffer);
            return dto.isEndTransmission();
        }
        return true;
    }


    private void SetFeedsToFeedTable(AppFeed appFeed) throws IOException, InterruptedException {

        AppFeed top = appFeeds.stream().filter(x -> x.getId() == appFeed.getId()).findFirst().orElse(null);
        if (top == null) appFeeds.add(appFeed);
    }

    private void SetTopicsToTopicTable(Topic topic) {
        Topic top = topics.stream().filter(x -> x.getId() == topic.getId()).findFirst().orElse(null);
        if (top == null) topics.add(topic);
    }

    public static byte[] convertToBytes(Object object) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        }
    }

    public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }
    private void ProcessDto(DataTransferObject dto, ByteBuffer byteBuffer) throws IOException, InterruptedException {
        switch (dto.getTransferCode()) {
            case 1: {
                SetTopicsToTopicTable((Topic) dto.getTransferObject());
                TopicTable.setItems(FXCollections.observableArrayList(topics));
                byteBuffer.clear();
                break;
            }
            case 4: {
                SetFeedsToFeedTable((AppFeed) dto.getTransferObject());
                FeedTable.setItems(FXCollections.observableArrayList(appFeeds));
                byteBuffer.clear();
                break;
            }
            case 10:
            {
                topics.clear();
                TopicTable.setItems(FXCollections.observableArrayList(topics));
            }
            case 11:
            {
                appFeeds.clear();
                FeedTable.setItems(FXCollections.observableArrayList(appFeeds));
            }
        }
    }
}
