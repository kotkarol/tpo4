package Models;

import java.io.Serializable;
import java.util.ArrayList;

public class AppFeed implements Serializable {

    public static ArrayList<AppFeed> AppFeedExtend = new ArrayList<>();

    private int id;
    private Topic feedTopic;
    private String feedTitle;
    private String feedContent;

    public AppFeed(int id, Topic feedTopic, String feedTitle, String feedContent)
    {
        this.id = id;
        this.feedTopic = feedTopic;
        this.feedTitle = feedTitle;
        this.feedContent = feedContent;

        AppFeedExtend.add(this);
    }

    public int getId() {
        return id;
    }

    public Topic getFeedTopic() {
        return feedTopic;
    }

    public void setFeedTopic(Topic feedTopic) {
        this.feedTopic = feedTopic;
    }

    public String getFeedTitle() {
        return feedTitle;
    }

    public void setFeedTitle(String feedTitle) {
        this.feedTitle = feedTitle;
    }

    public String getFeedContent() {
        return feedContent;
    }

    public void setFeedContent(String feedContent) {
        this.feedContent = feedContent;
    }
}
