package Models;

import Controllers.Tools;

import javax.tools.Tool;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;

public class Subscription {

    public static HashMap<SocketChannel, ArrayList<Topic>> SubscriptionExtend = new HashMap<>();
    private SocketChannel channel;
    private Topic topic;

    public Subscription(SocketChannel channel, Topic topic) {
        this.channel = channel;
        this.topic = topic;
        AddSubscriptionToExtend(channel, topic);
    }
    public static boolean hasSubscriptionForTopic(SocketChannel channel, Topic topic)
    {
        if(!SubscriptionExtend.containsKey(channel)) return false;
        ArrayList<Topic> topics = SubscriptionExtend.get(channel);
        return topics.stream().anyMatch(x -> x.getId() == topic.getId());
    }
    public static void DeleteSubscription(SocketChannel channel, Topic topic)
    {
        if(!hasSubscriptionForTopic(channel, topic)) return;
        Topic foundTopic = Topic.TopicExtend.stream().filter(x -> x.getId() == topic.getId()).findFirst().orElse(null);
        if(foundTopic == null) return;
        SubscriptionExtend.get(channel).remove(foundTopic);
    }
    public static void AddSubscription(SocketChannel channel, Topic topic)
    {
        if(hasSubscriptionForTopic(channel, topic)) return;
        Topic foundTopic = Topic.TopicExtend.stream().filter(x -> x.getId() == topic.getId()).findFirst().orElse(null);
        if(foundTopic == null) return;
        new Subscription(channel, topic);
    }
    public static ArrayList<SocketChannel> GetSubscribers(Topic topic)
    {
        ArrayList<SocketChannel> arrayList = new ArrayList<>();
        for (SocketChannel channel : SubscriptionExtend.keySet())
        {
            boolean result = SubscriptionExtend.get(channel).stream().anyMatch(x -> x == topic);
            if(result) arrayList.add(channel);
        }
        return arrayList;
    }
    public static void DeleteTopicFromAllSubscriptions(Topic topic) throws IOException {
        for (SocketChannel channel : GetSubscribers(topic)) {
            ByteBuffer byteBuffer = ByteBuffer.wrap(Tools.convertToBytes(new DataTransferObject(12, topic,true)));
            SubscriptionExtend.get(channel).remove(topic);
            for(Object appFeed : AppFeed.AppFeedExtend.stream().filter(x -> x.getFeedTopic() == topic).toArray())
            {
                AppFeed current = (AppFeed) appFeed;
                DeleteAppFeedFromAllSubscriptions(current);
            }
            channel.write(byteBuffer);
        }
    }
    public static void DeleteAppFeedFromAllSubscriptions(AppFeed appFeed) throws IOException {
        for (SocketChannel channel : GetSubscribers(appFeed.getFeedTopic())) {
            ByteBuffer byteBuffer = ByteBuffer.wrap(Tools.convertToBytes(new DataTransferObject(13, appFeed,true)));
            channel.write(byteBuffer);
        }
    }

    private void AddSubscriptionToExtend(SocketChannel channel, Topic topic) {
        if(!SubscriptionExtend.containsKey(channel))
        {
            ArrayList<Topic> topics = new ArrayList<>();
            topics.add(topic);
            SubscriptionExtend.put(channel, topics);
        }
        else
        {
            ArrayList<Topic> topics = SubscriptionExtend.get(channel);
            if(topics.stream().noneMatch(x -> x == topic)){
                topics.add(topic);
            }
        }
    }

    public SocketChannel getChannel() {
        return channel;
    }
    public Topic getTopic() {
        return topic;
    }
}
