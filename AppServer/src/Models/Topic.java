package Models;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Topic implements Serializable {

    public static ArrayList<Topic> TopicExtend = new ArrayList<>();

    private int id;
    private String name;

    public Topic(int id, String name) {
        this.id = id;
        this.name = name;

        TopicExtend.add(this);
    }

    public String getName() {
        return this.name;
    }
    public int getId() {
        return this.id;
    }
    public void setName(String value) {
        this.name = value;
    }
    public void setId(int value) {
        this.id = value;
    }

    @Override
    public String toString() {
        return name;
    }

    private int getMaxId() {
        TopicExtend.sort(Comparator.comparingInt(Topic::getId));
        int maxId = -1;
        if (TopicExtend.size() == 0) {
            maxId = 0;
        } else {
            maxId = TopicExtend.get(TopicExtend.size() - 1).getId();
        }
        return maxId;
    }

}
