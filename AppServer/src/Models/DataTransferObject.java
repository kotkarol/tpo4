package Models;

import java.io.Serializable;

public class DataTransferObject implements Serializable {
    private int transferCode;
    private Object transferObject;
    private boolean endTransmission;

    public DataTransferObject(int transferCode, Object transferObject, boolean endTransmission) {
        this.transferCode = transferCode;
        this.transferObject = transferObject;
        this.endTransmission = endTransmission;
    }
    public DataTransferObject(int transferCode, boolean endTransmission) {
        this.transferCode = transferCode;
        this.endTransmission = endTransmission;
    }

    public int getTransferCode() {
        return transferCode;
    }
    public Object getTransferObject() {
        return transferObject;
    }

    public boolean isEndTransmission() {
        return endTransmission;
    }
    public void setEndTransmission(boolean endTransmission) {
        this.endTransmission = endTransmission;
    }
}
