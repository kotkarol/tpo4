package AppServer;

import Controllers.Tools;
import Models.AppFeed;
import Models.DataTransferObject;
import Models.Subscription;
import Models.Topic;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static final int Port = 8090;

    private Selector selector;
    private Map<SocketChannel, List> dataMapper;
    private InetSocketAddress listenAddress;

    public static void main(String[] args) {
        Runnable server = () -> {
            try {
                new Main("localhost", Port).startServer();
            } catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        };
        new Thread(server).start();
    }

    public Main(String address, int port) throws IOException {
        listenAddress = new InetSocketAddress(address, port);
        dataMapper = new HashMap<>();
    }

    public void startServer() throws IOException, ClassNotFoundException {
        this.selector = Selector.open();
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        serverChannel.socket().bind(listenAddress);
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);
        while (true) {
            this.selector.select();
            Iterator iterator = this.selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = (SelectionKey) iterator.next();
                iterator.remove();
                if (!key.isValid()) continue;
                if (key.isAcceptable()) this.accept(key);
                if (key.isReadable()) this.read(key);
            }
        }
    }

    private void read(SelectionKey key) throws IOException, ClassNotFoundException {
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int numRead = -1;

        numRead = channel.read(buffer);
        if (numRead == -1) {
            this.dataMapper.remove(channel);
            Socket socket = channel.socket();
            SocketAddress remoteAddress = socket.getRemoteSocketAddress();
            channel.close();
            key.cancel();
            return;
        }
        Object object = convertFromBytes(buffer.array());
        DataTransferObject dto = (DataTransferObject)object;
        int transferCode = dto.getTransferCode();
        //Opisy wysyłanych kodów oraz operacji przeprowadzonych:
        //1 - Get all serverTopics
        //2 - Delete topic with given id
        //3 - Add new topic
        //4 - Get all feeds
        //5 - Delete feed with given id
        //6 - Add new feed
        //7 - Subscribe to topic
        //8 - Unsubscribe from topic
        //9 - Sends feeds to subcriber
        //10 - send message clear topics
        //11 - send message clear app feeds

        switch (transferCode) {
            case 1:
                SendAllTopics(channel);
                break;
            case 2:
                DeleteTopicWithGivenId((Topic)dto.getTransferObject());
                break;
            case 3:
                AddNewTopic((Topic)dto.getTransferObject());
                break;
            case 4:
                SendAllAppFeeds(channel);
                break;
            case 5:
                DeleteAppFeedWithGivenId((AppFeed) dto.getTransferObject());
                break;
            case 6:
                AddNewAppFeed((AppFeed) dto.getTransferObject());
                break;
            case 7:
                SubscribeToTopic((Topic) dto.getTransferObject(), channel);
                break;
            case 8:
                UnsubscribeFromTopic((Topic) dto.getTransferObject(), channel);
            case 9:
                SendAllFeedsToSubscriber(channel);
                break;
        }
    }

    private String getMessageType(String message) {
        StringBuilder allDigits = new StringBuilder();
        for (char c : message.toCharArray()) {
            if (Character.isDigit(c)) allDigits.append(c);
            if (c == ')') break;
        }
        return allDigits.toString();
    }

    private void SendAllFeedsToSubscriber(SocketChannel channel) throws IOException {
        ArrayList<DataTransferObject> arrayList = new ArrayList<>();
        for (AppFeed appFeed : AppFeed.AppFeedExtend) {
            if (!Subscription.hasSubscriptionForTopic(channel, appFeed.getFeedTopic())) continue;
            DataTransferObject dataTransferObject = new DataTransferObject(4 , appFeed, false);
            arrayList.add(dataTransferObject);
        }
        if(channel.isOpen())
        {
            if(arrayList.size() > 0)
            {
                arrayList.get(arrayList.size() - 1).setEndTransmission(true);
                channel.write(ByteBuffer.wrap((convertToBytes(arrayList))));
            }
            else
            {
                channel.write(ByteBuffer.wrap((convertToBytes(new DataTransferObject(11 , true)))));
            }
        }
    }

    /**
     * Method usubscribe channel from topic given in message.
     *
     * @param channel
     */
    private void UnsubscribeFromTopic(Topic topic, SocketChannel channel) {
        Subscription.DeleteSubscription(channel, topic);
    }
    /**
     * Method adds subscriber to topic name given in message
     */
    private void SubscribeToTopic(Topic topic, SocketChannel channel) {
        Subscription.AddSubscription(channel, topic);
    }

    private void AddNewAppFeed(AppFeed appFeed) {
        if(AppFeed.AppFeedExtend.contains(appFeed))return;
        AppFeed.AppFeedExtend.add(appFeed);
    }

    private void DeleteAppFeedWithGivenId(AppFeed appFeed) throws IOException {
        AppFeed foundAppFeed = AppFeed.AppFeedExtend.stream().filter(x -> x.getId() == appFeed.getId()).findFirst().orElse(null);
        if(foundAppFeed == null) return;
        Subscription.DeleteAppFeedFromAllSubscriptions(foundAppFeed);
        AppFeed.AppFeedExtend.remove(foundAppFeed);
    }

    private void SendAllAppFeeds(SocketChannel channel) throws IOException {
        ArrayList<DataTransferObject> arrayList = new ArrayList<>();
        for (AppFeed appFeed : AppFeed.AppFeedExtend) {
            DataTransferObject dataTransferObject = new DataTransferObject(4 , appFeed, false);
            arrayList.add(dataTransferObject);
        }
        if(channel.isOpen())
        {
            if(arrayList.size() > 0)
            {
                arrayList.get(arrayList.size() - 1).setEndTransmission(true);
                channel.write(ByteBuffer.wrap((convertToBytes(arrayList))));
            }
            else
            {
                channel.write(ByteBuffer.wrap((convertToBytes(new DataTransferObject(11 , true)))));
            }
        }
    }

    private void AddNewTopic(Topic topic) {
        if(Topic.TopicExtend.contains(topic)) return;
        Topic.TopicExtend.add(topic);
    }

    private void DeleteTopicWithGivenId(Topic topic) throws IOException {
        Topic foundTopic = Topic.TopicExtend.stream().filter(x -> x.getId() == topic.getId()).findFirst().orElse(null);
        if(foundTopic == null) return;
        Topic.TopicExtend.remove(foundTopic);
        Subscription.DeleteTopicFromAllSubscriptions(foundTopic);
    }

    /**
     * This method sends all topics available at server to given SocketChannel parameter.
     *
     * @param channel
     * @throws IOException
     */
    private void SendAllTopics(SocketChannel channel) throws IOException, ClassNotFoundException {
        ArrayList<DataTransferObject> arrayList = new ArrayList<>();
        for (Topic topic : Topic.TopicExtend) {
            DataTransferObject dataTransferObject = new DataTransferObject(1 , topic, false);
            arrayList.add(dataTransferObject);
        }
        if(channel.isOpen())
        {
            if(arrayList.size() > 0)
            {
                arrayList.get(arrayList.size() - 1).setEndTransmission(true);
                channel.write(ByteBuffer.wrap((convertToBytes(arrayList))));
            }
            else
            {
                channel.write(ByteBuffer.wrap((convertToBytes(new DataTransferObject(10, true)))));
            }
        }
    }

    private void accept(SelectionKey key) {
        try {
            ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
            SocketChannel channel = serverChannel.accept();
            channel.configureBlocking(false);
            dataMapper.put(channel, new ArrayList());
            channel.register(this.selector, SelectionKey.OP_READ);

            System.out.println("Client count: " + dataMapper.size());

        } catch (IOException | IllegalArgumentException ex) {
            ex.printStackTrace();
        }

    }

    public static boolean IsPatternMatchedInWord(String patternRegex, String word) {
        Pattern pattern = Pattern.compile(patternRegex);
        Matcher matcher = pattern.matcher(word);
        return matcher.matches();
    }
    public static byte[] convertToBytes(Object object) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        }
    }
    public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }

}
