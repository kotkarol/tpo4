package Controllers;

import Main.Main;
import Models.AppFeed;
import Models.DataTransferObject;
import Models.Topic;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import sun.rmi.runtime.Log;

import javax.xml.crypto.Data;
import javax.xml.soap.Text;
import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClientMainWindowController {
    public static String newLine = System.getProperty("line.separator");

    @FXML
    private TableView<Topic> TopicTable;
    @FXML
    public TableColumn<Topic, Integer> id;
    @FXML
    public TableColumn<Topic, String> name;
    @FXML
    private TableView<AppFeed> FeedTable;
    @FXML
    public TableColumn<AppFeed, Integer> feedId;
    @FXML
    public TableColumn<AppFeed, String> feedName;
    @FXML
    public TextArea LogArea;

    private ArrayList<Topic> topics = new ArrayList<>();
    private ArrayList<AppFeed> appFeeds = new ArrayList<>();

    @FXML
    public void initialize() throws IOException, InterruptedException {
        new Thread(getRunnableForServer()).start();

        id.setCellValueFactory(new PropertyValueFactory<>("Id"));
        name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        feedId.setCellValueFactory(new PropertyValueFactory<>("Id"));
        feedName.setCellValueFactory(new PropertyValueFactory<>("FeedTitle"));
        //add your data to the table here.
        LogArea.setEditable(false);
        //10 linii

        Tools.SendMessageToServer(Tools.MessageCodes.GetAllTopics, "");
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllSubscribedTopics, "");
    }

    public void OnSubscribeClick() throws IOException, InterruptedException {
        Topic selectedTopic = TopicTable.getSelectionModel().getSelectedItem();
        if (selectedTopic == null) return;
        Tools.SendMessageToServer(Tools.MessageCodes.SubscribeToTopic, selectedTopic);
    }

    public void OnUnsubscribeClick() throws IOException, InterruptedException {
        Topic selectedTopic = TopicTable.getSelectionModel().getSelectedItem();
        if (selectedTopic == null) return;
        Tools.SendMessageToServer(Tools.MessageCodes.UnsubscribeTopic, selectedTopic);
    }

    /**
     * `
     * Refreshes Topic and Feed table
     */
    public void RefreshClick() throws IOException, InterruptedException {
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllTopics, "");
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllSubscribedTopics, "");
    }

    public void RefreshClickExternal() throws IOException, InterruptedException {
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllTopics, "");
        Tools.SendMessageToServer(Tools.MessageCodes.GetAllSubscribedTopics, "");
    }

    private void LogChanges(Set<AppFeed> onlyNewFeeds, String message) {
        if (onlyNewFeeds.stream().findAny().isPresent()) {
            StringBuilder sb = new StringBuilder();
            for (AppFeed feed :
                    onlyNewFeeds) {
                sb.append(LocalDateTime.now()).append(message).append(feed.getFeedTitle()).append("\n");
            }
            LogArea.setText("");
        }
    }

    private <T> Set<T> symmetricDifference(ObservableList<T> a, ObservableList<T> b) {
        Set<T> result = new HashSet<>(a);
        for (T element : b) {
            if (result.contains(element)) {
                result.remove(element);
            }
        }
        return result;
    }

    private Runnable getRunnableForServer() {
        return () -> {
            try {
                InetSocketAddress hostAddress = new InetSocketAddress("localhost", 8090);
                Main.client = SocketChannel.open(hostAddress);
                while (true) {
                    ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
                    if (!Main.client.isConnected() || !Main.client.isOpen()) continue;
                    while (Main.client.read(byteBuffer) > 0) {
                        String message = new String(byteBuffer.array());
                        Object objectFromServer = convertFromBytes(byteBuffer.array());
                        boolean result = ProcessObjectFromServer(objectFromServer, byteBuffer);
                        if (result) break;
                    }
                }
            } catch (IOException | InterruptedException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        };
    }

    private void SetFeedsToFeedTable(AppFeed appFeed) throws IOException, InterruptedException {
        AppFeed top = appFeeds.stream().filter(x -> x.getId() == appFeed.getId()).findFirst().orElse(null);
        if (top == null) appFeeds.add(appFeed);
        addNewLineToLog("Topic add - ID:" + appFeed.getId() + " - Title: " + appFeed.getFeedTitle());
    }

    private void SetTopicsToTopicTable(Topic topic) {
        Topic top = topics.stream().filter(x -> x.getId() == topic.getId()).findFirst().orElse(null);
        if (top == null) topics.add(topic);
        addNewLineToLog("Topic add - ID:" + topic.getId() + " - Name: " + topic.getName());
    }



    public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }

    private boolean ProcessObjectFromServer(Object objectFromServer, ByteBuffer byteBuffer) throws IOException, InterruptedException {

        if (objectFromServer instanceof ArrayList<?>) {
            ArrayList<DataTransferObject> arrayList = (ArrayList<DataTransferObject>) objectFromServer;
            for (DataTransferObject dto : arrayList) {
                ProcessDto(dto, byteBuffer);
            }
            return true;
        } else if (objectFromServer instanceof DataTransferObject) {
            DataTransferObject dto = (DataTransferObject) objectFromServer;
            ProcessDto(dto, byteBuffer);
            return true;
        }
        return true;
    }

    private void ProcessDto(DataTransferObject dto, ByteBuffer byteBuffer) throws IOException, InterruptedException {
        switch (dto.getTransferCode()) {
            case 1: {
                SetTopicsToTopicTable((Topic) dto.getTransferObject());
                TopicTable.setItems(FXCollections.observableArrayList(topics));
                byteBuffer.clear();
                break;
            }
            case 4: {
                SetFeedsToFeedTable((AppFeed) dto.getTransferObject());
                FeedTable.setItems(FXCollections.observableArrayList(appFeeds));
                byteBuffer.clear();
                break;
            }
            case 10: {
                topics.clear();
                TopicTable.setItems(FXCollections.observableArrayList(topics));
                break;
            }
            case 11: {
                appFeeds.clear();
                FeedTable.setItems(FXCollections.observableArrayList(appFeeds));
                break;
            }
            case 12: {
                Topic topic = (Topic)dto.getTransferObject();
                Topic firstFound = topics.stream().filter(x -> x.getId() == topic.getId() && x.getName().equals(topic.getName())).findFirst().orElse(null);
                if(firstFound == null) break;
                addNewLineToLog("Topic removed - ID:" + firstFound.getId() + " - Name: " + firstFound.getName());
                topics.remove(firstFound);
            }
            case 13: {
                AppFeed appFeed = (AppFeed) dto.getTransferObject();
                AppFeed firstFound = appFeeds.stream().filter(x -> x.getId() == appFeed.getId() && x.getFeedTitle().equals(appFeed.getFeedTitle())).findFirst().orElse(null);
                if(firstFound == null) break;
                addNewLineToLog("AppFeed removed - ID:" + firstFound.getId() + " - Title: " + firstFound.getFeedTitle());
                appFeeds.remove(firstFound);
            }
        }
    }
    public  void addNewLineToLog(String message)
    {
        String[] splitted = LogArea.getText().split(newLine);
        String newtext = message + newLine + LogArea.getText();
        LogArea.setText("");
        LogArea.clear();
        LogArea.setText(newtext);
    }
}
