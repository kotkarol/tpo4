package Controllers;

import Main.Main;
import Models.DataTransferObject;
import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Tools {
    public static boolean isHostAvailaible(String serverAddress, int serverPort) {
        try (Socket s = new Socket(serverAddress, serverPort)) {
            return true;
        } catch (IOException ex) {
            /* ignore */
        }
        return false;
    }
    public static byte[] convertToBytes(Object object) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        }
    }

    public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }
    public static void SendMessageToServer(MessageCodes messageCode, Object transferObject) throws IOException, InterruptedException {
        DataTransferObject dto;
        switch (messageCode) {
            case GetAllTopics:
                dto = new DataTransferObject(1, true);
                break;
            case DeleteTopic:
                dto = new DataTransferObject(2, transferObject, true);
                break;
            case AddNewTopic:
                dto = new DataTransferObject(3, transferObject, true);
                break;
            case GetAllFeeds:
                dto = new DataTransferObject(4, true);
                break;
            case DeleteFeed:
                dto = new DataTransferObject(5, transferObject, true);
                break;
            case AddNewFeed:
                dto = new DataTransferObject(6, transferObject, true);
                break;
            case SubscribeToTopic:
                dto = new DataTransferObject(7, transferObject, true);
                break;
           case UnsubscribeTopic:
               dto = new DataTransferObject(8, transferObject, true);
                break;
            case GetAllSubscribedTopics:
                dto = new DataTransferObject(9, true);
                break;
            default:
                return;
        }
        ByteBuffer buffer = ByteBuffer.wrap(convertToBytes(dto));
        Main.client.write(buffer);
        buffer.clear();
    }
    public enum MessageCodes {
        GetAllTopics,
        DeleteTopic,
        AddNewTopic,
        GetAllFeeds,
        DeleteFeed,
        AddNewFeed,
        SubscribeToTopic,
        UnsubscribeTopic,
        GetAllSubscribedTopics
    }
}
