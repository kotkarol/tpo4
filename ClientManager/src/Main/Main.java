package Main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class Main extends Application {

    public static InetSocketAddress hostAddress;
    public static SocketChannel client;
    public static Selector selector;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
        primaryStage.setTitle("TPO 4 - Client Manager");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();

        primaryStage.setOnCloseRequest(x -> {
            if(client.isOpen()) {
                try {
                    client.finishConnect();
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public static void main(String[] args) throws IOException {
        Runnable server = getRunnableForServer();
        Runnable ui = () -> {
          launch(args);
        };
        Thread serverThread = new Thread(server);
        Thread uiThread = new Thread(ui);
        serverThread.start();
        uiThread.start();
    }

    private static Runnable getRunnableForServer() {
        return () -> {
                try {
                    hostAddress = new InetSocketAddress("localhost", 8090);
                    client = SocketChannel.open(hostAddress);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            };
    }
}
