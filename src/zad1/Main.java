package zad1;

import javafx.application.Application;


import java.io.IOException;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Runnable server = () -> {
            try {
                new AppServer.Main("localhost", 8090).startServer();
            } catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        };
        new Thread(() -> Application.launch(ContentManager.Main.class)).start();
        new Thread(server).start();
    }
}
